# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v0.4.2]

## Changed

* Fix publish CI job [#44](https://gitlab.com/coopdevs/pyopencell/-/merge_requests/44)

## [v0.4.1]

## Add

* Add the CustomerAccount resource [#41](https://gitlab.com/coopdevs/pyopencell/-/merge_requests/41)

## [v0.4.0]

## Add

* Add subscription list endpoint [#40](https://gitlab.com/coopdevs/pyopencell/-/merge_requests/40)

## [v0.3.2]

* Raise error if status fail [#39](https://gitlab.com/coopdevs/pyopencell/merge_requests/39)

## [v0.3.1]

## Add

* Upload package to PyPI with Gitlab-ci [#37](https://gitlab.com/coopdevs/pyopencell/merge_requests/37)
* Add to PyOpenCellAPIException an attribute with the API body error [#38](https://gitlab.com/coopdevs/pyopencell/merge_requests/38)
* Add method to send an invoice by email [#36](https://gitlab.com/coopdevs/pyopencell/merge_requests/36)
